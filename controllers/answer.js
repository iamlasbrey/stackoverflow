const { Answer } = require('../models');
const { Question } = require('../models');

// eslint-disable-next-line consistent-return
const postAnswer = async (req, res) => {
    const questID = req.params.questionId;
    try {
// Fetch the Question to make sure it exists
const findQuestion = await Question.findByPk(questID);

// Error if question does not exist
if (!findQuestion) {
    return res.status(404).json({
        status: 'error',
        message: 'This question doesn\'t exist',
    });
}

// Create a new answer
        const newAnswer = await Answer.create({
            answer: req.body.answer,
            questionId: findQuestion.questionId,
        });
        return res.status(201).json({
            status: 'success',
            message: 'answer has been posted',
            data: newAnswer,
        });
    } catch (error) {
        return res.status(404).json({ status: 'error', error: error.message });
    }
};

const getAnswers = async (req, res) => {
    const qId = req.params.questionId;
    try {
        // Fetch the Question to make sure it exists
        const findQuestion = await Question.findByPk(qId);

        if (!findQuestion) {
            return res.status(400).json({
            status: 'error',
            error: 'This Question does not exist',
        });
    }

        // Find all answers to the question
            const findAllAnswers = await Answer.findAll({
                    where: { questionId: qId },
                });
                return res.status(200).json({
                    status: 'success',
                    message: 'answers fetched successfully',
                    data: findAllAnswers,
                });
            } catch (error) {
                return res.status(404).json({ status: 'error', error: error.message });
            }
};

// eslint-disable-next-line consistent-return
const upVoteFunction = async (req, res) => {
    const { answerId } = req.params;
    const { userId } = req.user;

        // Fetch the Question to make sure it exists
        try {
            const findAnswer = await Answer.findByPk(answerId);
            if (!findAnswer) return res.status(400).json({ status: 'error', error: 'This answer doesn\'t exist' });

            // Check if user has already upvoted before
            const isUserInUpvotesMeta = findAnswer.upvotesmeta.includes(userId);
            if (isUserInUpvotesMeta) {
                return res.status(400).json({ status: 'error', error: 'user already has upvoted' });
            }

            // Turn downvotes to false before turning upvote to true
            const isUserInDownvotesMeta = findAnswer.downvotesmeta.includes(userId);
            if (isUserInDownvotesMeta) {
                const result = await Answer.decrement('downvotes', {
                    by: 1,
                    where: { answerId },
                  });
            }

            if (isUserInDownvotesMeta) {
                const downvotesMetaArray = findAnswer.downvotesmeta;
                const targetId = userId;

                // Check if the array contains the target ID
                const indexOfTargetId = downvotesMetaArray.indexOf(targetId);
                if (indexOfTargetId !== -1) {
                  // Remove the target ID from the array
                  downvotesMetaArray.splice(indexOfTargetId, 1);
                  console.log('downvotesMetaArray:', downvotesMetaArray);

                  // Save the updated record
                  await Answer.update(
                    { downvotesmeta: downvotesMetaArray },
                    {
                        where: { answerId },
                    },
                    );
                  console.log(`ID ${targetId} removed from downvotesmeta.`);
                } else {
                  console.log(`ID ${targetId} not found in downvotesmeta.`);
                }
              } else {
                console.log('Record not found');
              }

              // Update upvote from user
            const updatedUpvotesmeta = [...findAnswer.upvotesmeta, userId];

                const [numRowsUpdated] = await Answer.update(
                    { upvotesmeta: updatedUpvotesmeta },
                    {
                    where: { answerId },
                    },
                );

            const result = await Answer.increment('upvotes', {
                by: 1,
                where: { answerId },
              });

              res.status(200).json({
                status: 'success',
                message: 'Answer has been upvoted',
             });
        } catch (error) {
            return res.status(404).json({ status: 'error', error: error.message });
        }
};

// eslint-disable-next-line consistent-return
// eslint-disable-next-line consistent-return
const downvoteFunction = async (req, res) => {
    const { answerId } = req.params;
    const { userId } = req.user;

        // Fetch the Question to make sure it exists
        try {
            const findAnswer = await Answer.findByPk(answerId);
            if (!findAnswer) return res.status(400).json({ status: 'error', error: 'This answer doesn\'t exist' });

            // Check if user has already upvoted before
            const isUserInDownvotesmeta = findAnswer.downvotesmeta.includes(userId);
            if (isUserInDownvotesmeta) {
                return res.status(400).json({ status: 'error', error: 'user already has downvoted' });
            }

            // Turn downvotes to false before turning upvote to true
            const isUserInUpvotesMeta = findAnswer.upvotesmeta.includes(userId);
            if (isUserInUpvotesMeta) {
                const result = await Answer.decrement('downvotes', {
                    by: 1,
                    where: { answerId },
                  });
            }

            if (isUserInUpvotesMeta) {
                const upvotesMetaArray = findAnswer.upvotesmeta;
                const targetId = userId;

                // Check if the array contains the target ID
                const indexOfTargetId = upvotesMetaArray.indexOf(targetId);
                if (indexOfTargetId !== -1) {
                  // Remove the target ID from the array
                  upvotesMetaArray.splice(indexOfTargetId, 1);
                  console.log('downvotesMetaArray:', upvotesMetaArray);

                  // Save the updated record
                  await Answer.update(
                    { downvotesmeta: upvotesMetaArray },
                    {
                        where: { answerId },
                    },
                    );
                  console.log(`ID ${targetId} removed from downvotesmeta.`);
                } else {
                  console.log(`ID ${targetId} not found in downvotesmeta.`);
                }
              } else {
                console.log('Record not found');
              }

              // Update downvote from user
            const updatedDownvotesmeta = [...findAnswer.upvotesmeta, userId];

                const [numRowsUpdated] = await Answer.update(
                    { downvotesmeta: updatedDownvotesmeta },
                    {
                    where: { answerId },
                    },
                );

            const result = await Answer.increment('downvotes', {
                by: 1,
                where: { answerId },
              });

              res.status(200).json({
                status: 'success',
                message: 'Answer has been downvoted',
             });
        } catch (error) {
            return res.status(404).json({ status: 'error', error: error.message });
        }
};

module.exports = {
 postAnswer, getAnswers, upVoteFunction, downvoteFunction,
};
