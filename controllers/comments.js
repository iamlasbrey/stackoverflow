const { Answer } = require('../models');
const { Comment } = require('../models');

// Create a new question
// eslint-disable-next-line consistent-return
const postComment = async (req, res) => {
    const { answerId } = req.params;

    try {
        const findAnswer = await Answer.findByPk(answerId);
        if (!findAnswer) {
            res.status(404).json({ status: 'error', error: 'Answer doesn\'t exist' });
        }

        const newComment = await Comment.create({
            comment: req.body.comment,
            answerId: findAnswer.answerId,
        });

        return res.status(201).json({
            status: 'success',
            message: 'answer has been posted',
            data: newComment,
        });
} catch (error) {
        return res.status(404).json({ status: 'error', error: error.message });
    }
};

module.exports = { postComment };
