const { User } = require('../models');
const { encrpytPassword, decrpytPasswordFunc } = require('../services/encryptPass');
const { emailExistsFunc } = require('../services/emailExists');
const generateJwtToken = require('../services/generateToken');

// Login into an existing account
const login = async (req, res) => {
    const {
        email,
        password,
    } = req.body;

    // Fetch email from the database
    try {
    const findEmail = await User.findOne({
        where: { email },
    });

    // Return an error if email doesn't exist
    if (!findEmail) {
        return res.status(401).json({
            status: 'error',
            error: 'This email doesn\'t exist',
        });
    }

    // descrypt the user password using cryptoJS library
    const userPass = decrpytPasswordFunc(findEmail.password);

    // eslint-disable-next-line max-len
    //  Test to check if the user password and databse password match and Return an error if password doesn't match
    if (password !== userPass) {
        return res.status(401).json({
            status: 'error',
            error: 'Credentials not correct',
        });
    }

        return res.status(200).json({
            status: 'success',
            message: 'account fetched successfully',
            data: {
                email,
                token: generateJwtToken(findEmail.userId),
            },
        });
     } catch (error) {
        return res.status(400).json({
            status: 'error',
            message: error,
        });
     }
};

// Register a new User
const register = async (req, res) => {
    const {
        firstName,
        lastName,
        email,
        password,
    } = req.body;

    try {
    // Checks if Email Already Exists in the database
    if (await emailExistsFunc(req.body.email)) {
        return res.status(400).json({
            status: 'error',
            error: 'Email address already exist',
        });
    }

    /* Encrypt the password using the cryptojs library */
    const encryptedpass = encrpytPassword(password);

    // create a create call to the database
    const newUser = await User.create({
         firstName,
         lastName,
         email,
         password: encryptedpass,
    });

       return res.status(201).json({
        status: 'success',
        message: 'account created successfully',
        data: {
            newUser,
            token: generateJwtToken(newUser.userId),
        },
       });
    } catch (error) {
        return res.status(400).json({
            status: 'failed',
            message: 'Account creation failed',
            error: error.message,
        });
    }
};

module.exports = {
    login,
    register,
};
