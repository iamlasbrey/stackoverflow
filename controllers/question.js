const { Question } = require('../models');
const { deleteAnswers } = require('../services/deleteAnswers');

// Create a new question
const createQuestion = async (req, res) => {
    try {
        const newQuestion = await Question.create({
            question: req.body.question,
            userId: req.user.userId,
        });
        return res.status(201).json({
            status: 'success',
            message: 'question created successfully',
            data: newQuestion,
        });
    } catch (error) {
        return res.status(404).json({
        status: 'error',
        error: error.message,
    });
    }
};

// Delete a question
const deleteQuestion = async (req, res) => {
    const questId = req.params.id;

    try {
        // fetch the question and confirm if the userId corresponds with userId from the middlware
        const questionToDelete = await Question.findByPk(questId);
        if (!questionToDelete) {
            return res.status(404).json({ status: 'error', error: 'Question Not Found, Please Try Again' });
        }

        // If user didn't post the question they cannot delete the question
        if (questionToDelete.userId !== req.user.userId) return res.status(401).json({ status: 'error', error: 'User is not allowed to delete this question' });

    // Delete the question
    await deleteAnswers(questId);
    await questionToDelete.destroy();
    return res.status(200).json({
        status: 'success',
        message: 'Question deleted',
    });
    } catch (error) {
        return res.status(404).json({ status: 'error', error: error.message });
    }
};

    // Get all user questions
    const getQuestions = async (req, res) => {
    const user = req.user.userId;

        try {
            const findAllQuestions = await Question.findAll({
                    where: { userId: user },
                });

         // If User doesn't doesn't have any question
        if (findAllQuestions.length < 1) {
            return res.status(200).json({
            status: 'error',
            message: 'User has no questions',
            data: findAllQuestions,
            });
        }

            return res.status(200).json({
                status: 'success',
                message: 'questions fetched successfully',
                data: findAllQuestions,
            });
        } catch (error) {
            return res.status(404).json({ status: 'error', error: error.message });
        }
    };

module.exports = { createQuestion, deleteQuestion, getQuestions };
