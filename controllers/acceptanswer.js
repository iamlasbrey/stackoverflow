const { Answer } = require('../models');
const { Question } = require('../models');

// eslint-disable-next-line consistent-return
const acceptAnswer = async (req, res) => {
    const { answerId } = req.params;

    try {
        // Confirm if the answer exists in the database
        const findAnswer = await Answer.findByPk(answerId);

        if (!findAnswer) {
            return res.status(400).json({
                status: 'error',
                error: 'This answer doesn\'t exist',
             });
        }

        // Fetch the User Account to test if the user can set preferred answer
        const findQuestion = await Question.findOne({
            where: {
                questionId: findAnswer.questionId,
            },
        });

        if (findQuestion.userId !== req.user.userId) {
            return res.status(400).json({
                status: 'error',
                error: 'You are not allowed to make this change',
             });
        }

        if (!findAnswer) {
            return res.status(400).json({
                status: 'error',
                message: 'This answer doesn\'t exist in the database',
            });
        }

        // Check if answer is already preferred
        if (findAnswer.preferred === true) {
                return res.status(400).json({
                    status: 'error',
                    message: 'This answer is already the preferred answer',
                });
            }

            // Check if any other answer is already preferred
            const findPreferred = await Answer.findAll({
                where: {
                questionId: findAnswer.questionId,
                preferred: true,
                },
            });

            if (findPreferred) {
                await Answer.update(
                    { preferred: false },
                    {
                      where: {
                        questionId: findAnswer.questionId,
                        preferred: true,
                      },
                    },
                  );
            }

        const setPreferredAnswer = await Answer.update(
            { preferred: true },
            { where: { answerId } },
          );

        return res.status(200).json({
            status: 'success',
            message: 'Answer has been set to preferred',
        });
    } catch (error) {
        return res.status(404).json({ status: 'error', error: error.message });
 }
};

module.exports = {
    acceptAnswer,
   };
