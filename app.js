const express = require('express');
const { sequelize } = require('./models');

const app = express();
// eslint-disable-next-line import/order
const logger = require('morgan');
require('dotenv').config();
const AuthRoute = require('./routes/auth');
const QuestionRoute = require('./routes/Questions');
const AnswerRoute = require('./routes/answers');
const commentRoute = require('./routes/comment');
const acceptAnswerRoute = require('./routes/acceptanswer');
const { connectDB } = require('./connect');

app.use(express.json());
app.use(logger('dev'));

  // Routes
  app.get('/', (req, res) => {
    res.send('<h1>Welcome to StackOverFlow 2.0</h1>');
  });

  // Auth Route
  app.use('/api/v1/auth', AuthRoute);

  // Questions Route
  app.use('/api/v1/questions', QuestionRoute);

  // Answers Route
  app.use('/api/v1/questions', AnswerRoute);

  // Accept Answer Route
  app.use('/api/v1/answers', acceptAnswerRoute);

  // Comments Route
  app.use('/api/v1/answers', commentRoute);

    app.listen(process.env.PORT, async () => {
        console.log(`Server is running on port ${process.env.PORT}`);
        await connectDB();
    });

    module.exports = {
      app,
    };
