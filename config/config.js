require('dotenv').config();

// `postgres://${process.env.DATABASE_USERNAME}:${process.env.DATABASE_PASSWORD}@${process.env.DATABASE_HOST}/s${process.env.DATABASE}`

module.exports = {
    development: {
      username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASS,
    database: process.env.TEST_DATABASE,
    host: process.env.HOST,
    dialect: 'mysql',
     },

  test: {
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASS,
    database: process.env.TEST_DATABASE,
    host: process.env.HOST,
    dialect: 'mysql',
  },
  production: {
    use_env_variable: `postgres://${process.env.DATABASE_USERNAME}:${process.env.DATABASE_PASS}@${process.env.HOST}/${process.env.DATABASE}`,
    dialect: 'postgres',
    dialectOptions: {
       ssl: {
         require: true,
         rejectUnauthorized: false,
      },
    },
   },
};
