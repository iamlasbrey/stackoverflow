const { registerSchema, loginSchema } = require('../schema/userSchema');
const { questionSchema } = require('../schema/questionSchema');
const { answerSchema } = require('../schema/answerSchema');
const { commentSchema } = require('../schema/commentSchema');

function registerValidate(req, res, next) {
  // Remove white spaces becasue it seems the Joi isn't working
  Object.keys(req.body).forEach((key) => {
    if (typeof req.body[key] === 'string') {
      req.body[key] = req.body[key].trim();
    }
  });
  const { error } = registerSchema.validate(req.body);
  if (error) return res.status(400).json({ status: 'error', error: error.message });
  return next();
}

function loginValidate(req, res, next) {
  // Remove white spaces becasue it seems the Joi isn't working
  Object.keys(req.body).forEach((key) => {
    if (typeof req.body[key] === 'string') {
      req.body[key] = req.body[key].trim();
    }
  });
    const { error } = loginSchema.validate(req.body);
    if (error) {
      return res.status(400).json({ status: 'error', error: error.message });
    }
    return next();
  }

  function questionValidate(req, res, next) {
    // Remove white spaces becasue it seems the Joi isn't working
    Object.keys(req.body).forEach((key) => {
      if (typeof req.body[key] === 'string') {
        req.body[key] = req.body[key].trim();
      }
    });
    const { error } = questionSchema.validate(req.body);
    if (error) {
      return res.status(400).json({
        status: 'error',
        error: error.message,
      });
    }
    return next();
  }

  function answerValidate(req, res, next) {
    // Remove white spaces becasue it seems the Joi isn't working
    Object.keys(req.body).forEach((key) => {
      if (typeof req.body[key] === 'string') {
        req.body[key] = req.body[key].trim();
      }
    });
    const { error } = answerSchema.validate(req.body);
    if (error) {
      return res.status(400).json({
        status: 'error',
        error: error.message,
      });
    }
    return next();
  }

  function commentValidate(req, res, next) {
    // Remove white spaces becasue it seems the Joi isn't working
    Object.keys(req.body).forEach((key) => {
      if (typeof req.body[key] === 'string') {
        req.body[key] = req.body[key].trim();
      }
    });
    const { error } = commentSchema.validate(req.body);
    if (error) {
      return res.status(400).json({
        status: 'error',
        error: error.message,
      });
    }
    return next();
  }

module.exports = {
  registerValidate,
  loginValidate,
  questionValidate,
  answerValidate,
  commentValidate,
};
