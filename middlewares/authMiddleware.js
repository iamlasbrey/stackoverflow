const jwt = require('jsonwebtoken');
const { User } = require('../models');

const authMiddlewareService = async (req, res, next) => {
    let token;

    try {
        if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
            // Extract the token from the authorization header
            // eslint-disable-next-line prefer-destructuring
            token = req.headers.authorization.split(' ')[1];

            // Verify the token
            const decodedToken = jwt.verify(token, process.env.JWTPASS);

            // Extract user ID from the decoded token
            const userId = decodedToken.id;

            // Find the user by ID
            const user = await User.findOne({
                where: { userId },
            });

            if (!user) {
                return res.status(401).json({ status: 'error', error: 'Not Authorized, user not found' });
            }

            // Attach the user to the request object
            req.user = user;

            // Continue to the next middleware
            return next();
        }
    } catch (error) {
        // Handle token verification errors
        console.error('Token verification error:', error.message);
        return res.status(401).json({ status: 'error', error: 'User Not Authorized' });
    }

    // If no token is found, return an unauthorized response
    return res.status(401).json({ status: 'error', error: 'Not Authorized, token not found' });
};

module.exports = authMiddlewareService;
