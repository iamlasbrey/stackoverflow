// eslint-disable-next-line strict, lines-around-directive
'use strict';

const { Sequelize } = require('sequelize');
const { Model } = require('sequelize');
const Question = require('./Question');
require('dotenv').config();

// const sequelize = new Sequelize();

// const sequelize = new Sequelize(
//     process.env.DATABASE,
//     process.env.DATABASE_USERNAME,
//     process.env.DATABASE_PASS,
//     {
//       host: '127.0.0.1',
//       dialect: 'mysql',
//     },
//   );

// sequelize.authenticate().then(() => {
//    console.log('Connection has been established successfully.');
// }).catch((error) => {
//    console.error('Unable to connect to the database: ', error.message);
// });

// module.exports = function (sequelize, DataTypes) {
// const Answers = sequelize.define('Answers', {
//     answerId: {
//         type: DataTypes.UUID,
//         defaultValue: Sequelize.UUIDV4,
//         primaryKey: true,
//       },
//     answer: {
//       type: DataTypes.STRING,
//       allowNull: true,
//       defaultValue: 0,
//       validate: {
//       },
//     },
//     upvotes: {
//         type: DataTypes.INTEGER,
//         allowNull: true,
//         defaultValue: 0,
//         validate: {
//             min: 0,
//       },
//     },
//     downvotes: {
//         type: DataTypes.INTEGER,
//         allowNull: true,
//         defaultValue: 0,
//         validate: {
//             min: 0,
//       },
//     },
//     upvotesmeta: {
//         type: DataTypes.JSON(),
//         allowNull: true,
//         defaultValue: [], // Default value is an empty array
//     },
//     downvotesmeta: {
//         type: DataTypes.JSON(),
//         allowNull: true,
//         defaultValue: [], // Default value is an empty array
//     },
//     preferred: {
//         type: DataTypes.BOOLEAN,
//         allowNull: true,
//         defaultValue: false, // Default value is false
//     },
//  });

//  Question.hasMany(Answers, {
//     foreignKey: 'questionId',
//  });
//  Answers.belongsTo(Question, {
//     foreignKey: 'questionId',
//  });

//  return Answers;
// };

module.exports = (sequelize, DataTypes) => {
  class Answer extends Model {
    // eslint-disable-next-line no-shadow
    static associate({ Answer, Question }) {
      this.belongsTo(Question, { foreignKey: 'questionId' });
      this.hasMany(Answer, { foreignKey: 'questionId' });
    }
  }

  Answer.init({
    answerId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    answer: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
      },
    },
    upvotes: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      validate: {
        min: 0,
      },
    },
    downvotes: {
              type: DataTypes.INTEGER,
              allowNull: true,
              defaultValue: 0,
              validate: {
                  min: 0,
            },
          },
        upvotesmeta: {
        type: DataTypes.JSON(),
        allowNull: true,
        defaultValue: [], // Default value is an empty array
    },
    downvotesmeta: {
        type: DataTypes.JSON(),
        allowNull: true,
        defaultValue: [], // Default value is an empty array
    },
    preferred: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false, // Default value is false
    },

  }, {
    sequelize,
    tableName: 'answers',
    modelName: 'Answer',
  });

  return Answer;
};
//  sequelize.sync({ force: false }).then(() => {
//   console.log('Answers table created successfully!');
// }).catch((error) => {
//   console.error('Unable to create table : ', error.message);
// });

// module.exports = Answers;
