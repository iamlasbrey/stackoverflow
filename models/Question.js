const { Model } = require('sequelize');
require('dotenv').config();

module.exports = (sequelize, DataTypes) => {
  class Question extends Model {
    // eslint-disable-next-line no-shadow
    static associate({ User, Question }) {
      this.belongsTo(User, { foreignKey: 'userId' });
      this.hasMany(Question, { foreignKey: 'userId' });
    }
  }

  Question.init({
    questionId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    question: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  }, {
    sequelize,
    tableName: 'questions',
    modelName: 'Question',
  });

  return Question;
};

// module.exports = function (DataTypes, sequelize) {
// const Question = sequelize.define('Question', {
//     questionId: {
//         type: DataTypes.UUID,
//         defaultValue: Sequelize.UUIDV4,
//         primaryKey: true,
//       },
//     question: {
//       type: DataTypes.STRING,
//       allowNull: false,
//       validate: {
//         notEmpty: true,
//       },
//     },
//  });

//  User.hasMany(Question, {
//     foreignKey: 'userId',
//  });
//  Question.belongsTo(User, {
//     foreignKey: 'userId',
//  });
//  return Question;
// };

//  sequelize.sync({ force: false }).then(() => {
//   console.log('Question table created successfully!');
// }).catch((error) => {
//   console.error('Unable to create table : ', error.message);
// });

//  module.exports = Question;
