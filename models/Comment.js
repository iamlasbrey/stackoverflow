const { Sequelize } = require('sequelize');
const { Model } = require('sequelize');
const { Answer } = require('./Answer');
// const sequelize = new Sequelize(
//     process.env.DATABASE,
//     process.env.DATABASE_USERNAME,
//     process.env.DATABASE_PASS,
//     {
//       host: '127.0.0.1',
//       dialect: 'mysql',
//     },
//   );

// sequelize.authenticate().then(() => {
//    console.log('Connection has been established successfully.');
// }).catch((error) => {
//    console.error('Unable to connect to the database: ', error.message);
// });

// module.exports = function (DataTypes, sequelize) {
// const Comment = sequelize.define('Comment', {
//     commentId: {
//         type: DataTypes.UUID,
//         defaultValue: Sequelize.UUIDV4,
//         primaryKey: true,
//       },
//     comment: {
//       type: DataTypes.STRING,
//       allowNull: false,
//       validate: {
//         notEmpty: true,
//       },
//     },
//  });

//  Answers.hasMany(Comment, {
//     foreignKey: 'answerId',
//  });
//  Comment.belongsTo(Answers, {
//     foreignKey: 'answerId',
//  });
//   return Comment;
// };

module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    // eslint-disable-next-line no-shadow
    static associate({ Answer, Comment }) {
      this.belongsTo(Answer, { foreignKey: 'answerId' });
      this.hasMany(Comment, { foreignKey: 'answerId' });
    }
  }

  Comment.init({
    commentId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    comment: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  }, {
    sequelize,
    tableName: 'comments',
    modelName: 'Comment',
  });

  return Comment;
};

//  sequelize.sync({ force: false }).then(() => {
//   console.log('Comment table created successfully!');
// }).catch((error) => {
//   console.error('Unable to create table : ', error.message);
// });

//  module.exports = Comment;
