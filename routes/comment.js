const express = require('express');

const router = express.Router();

const { postComment } = require('../controllers/comments');

const authMiddlewareService = require('../middlewares/authMiddleware');

const { commentValidate } = require('../middlewares/validationMiddleware');

router.post('/comment/:answerId', commentValidate, authMiddlewareService, postComment);

module.exports = router;
