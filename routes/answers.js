const express = require('express');

const router = express.Router();

const { answerValidate } = require('../middlewares/validationMiddleware');

const {
    postAnswer, getAnswers, upVoteFunction, downvoteFunction,
} = require('../controllers/answer');

const authMiddlewareService = require('../middlewares/authMiddleware');

router.post('/:questionId/answers', answerValidate, authMiddlewareService, postAnswer);

router.get('/:questionId/answers', getAnswers);

router.post('/upvote/:answerId', authMiddlewareService, upVoteFunction);

router.post('/downvote/:answerId', authMiddlewareService, downvoteFunction);

module.exports = router;
