const express = require('express');

const router = express.Router();

const { createQuestion, deleteQuestion, getQuestions } = require('../controllers/question');
const authMiddlewareService = require('../middlewares/authMiddleware');
const { questionValidate } = require('../middlewares/validationMiddleware');

router.get('/', authMiddlewareService, getQuestions);
router.post('/', authMiddlewareService, questionValidate, createQuestion);
router.delete('/:id', authMiddlewareService, deleteQuestion);

module.exports = router;
