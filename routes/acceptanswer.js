const express = require('express');
const { acceptAnswer } = require('../controllers/acceptanswer');
const authMiddlewareService = require('../middlewares/authMiddleware');

const router = express.Router();

router.post('/:answerId/accept', authMiddlewareService, acceptAnswer);

module.exports = router;
