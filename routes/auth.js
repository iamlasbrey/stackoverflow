const express = require('express');
const { registerValidate, loginValidate } = require('../middlewares/validationMiddleware');

const router = express.Router();

const { login, register } = require('../controllers/auth');

router.post('/login', loginValidate, login);
router.post('/register', registerValidate, register);

module.exports = router;
