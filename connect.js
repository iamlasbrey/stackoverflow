// const Sequelize = require('sequelize');
const { sequelize } = require('./models');
// const sequelize = new Sequelize(
//     process.env.DATABASE,
//     process.env.DATABASE_USERNAME,
//     process.env.DATABASE_PASS,
//     {
//       host: process.env.HOST,
//       dialect: process.env.DIALET,
//     },
//   );

const connectDB = async () => {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully');
    } catch (error) {
       console.error('error string,', error.message);
    }
};

module.exports = {
    connectDB, sequelize,
};
