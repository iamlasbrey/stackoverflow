/* eslint-disable no-console */
const { User } = require('../models');

async function emailExistsFunc(email) {
    let foundEmail = null;
    try {
        const findEmail = await User.findOne({
            where: { email },
        });
        foundEmail = findEmail;
    } catch (error) {
        console.warn(error);
    }
    return foundEmail;
}

module.exports = {
    emailExistsFunc,
};
