const jwt = require('jsonwebtoken');

const generateJwtToken = (id) => {
    const token = jwt.sign({ id }, process.env.JWTPASS, {
        expiresIn: '2d',
    });
    return token;
};

module.exports = generateJwtToken;
