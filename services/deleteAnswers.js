/* eslint-disable no-console */
const { Answer } = require('../models');

async function deleteAnswers(id) {
    try {
        const findAllAnswers = await Answer.findAll({
            where: { questionID: id },
        });

        if (findAllAnswers) {
            await Answer.destroy({
                where: { questionID: id },
            });
        }
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    deleteAnswers,
};
