const CryptoJS = require('crypto-js');

const encrpytPassword = (password) => {
    const hashedPassword = CryptoJS.AES.encrypt(password, process.env.HARSHPASS).toString();
    return hashedPassword;
};

const decrpytPasswordFunc = (hashedpass) => {
    const decrpytPassword = CryptoJS.AES.decrypt(hashedpass, process.env.HARSHPASS)
    .toString(CryptoJS.enc.Utf8);
    return decrpytPassword;
};

module.exports = {
    encrpytPassword,
    decrpytPasswordFunc,
};
