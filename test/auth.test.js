// /* eslint-disable no-undef */
// const chai = require('chai');
// const chaiHttp = require('chai-http');

// const { app } = require('../app'); // Assuming your Express app is in 'app.js' or another file

// chai.use(chaiHttp);
// const { expect } = chai;

// // eslint-disable-next-line no-undef
// describe('Authentication', () => {
//   // eslint-disable-next-line no-undef
//   it('should successfully log in with correct credentials', (done) => {
//     chai.request(app)
//       .post('/api/v1/auth/login') // Adjust the route if your login route is different
//       .send({
//         email: 'test6@test.com', // Provide valid credentials
//         password: 'ttttttt',
//       })
//       .end((err, res) => {
//         expect(res.body).to.have.property('status').equal('success');
//         expect(res.body).to.have.property('message').equal('account fetched successfully');
//         expect(res.body.data).to.have.property('email').equal('test6@test.com');
//         expect(res.body.data).to.have.property('token');
//         done();
//       });
//   });

//   it('should successfully register an account with correct credentials', (done) => {
//     chai.request(app)
//       .post('/api/v1/auth/register') // Adjust the route if your login route is different
//       .send({
//         firstName: 'test',
//         lastName: 'test',
//         email: 'test27@test.com',
//         password: 'ttttttt',
//       })
//       .end((err, res) => {
//         expect(res.body).to.have.property('status').equal('success');
//         expect(res.body).to.have.property('message').equal('account created successfully');
//         expect(res.body.data.newUser).to.have.property('userId');
//         expect(res.body.data).to.have.property('token');
//         expect(res.body.data.newUser).to.have.property('firstName');
//         expect(res.body.data.newUser).to.have.property('lastName');
//         expect(res.body.data.newUser).to.have.property('email');
//         expect(res.body.data.newUser).to.have.property('password');
//         done();
//       });
//   });
// });
