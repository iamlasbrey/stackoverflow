// /* eslint-disable no-undef */
// const chai = require('chai');
// const chaiHttp = require('chai-http');

// chai.use(chaiHttp);

// const { app } = require('../app');

// const { expect } = chai;

// // Sample user credentials for authentication
// const userCredentials = {
//   email: 'test6@test.com',
//   password: 'ttttttt',
// };

// let authToken; // Will store the authentication token
// let questionId; // Will store the questionId from a previous test

// // eslint-disable-next-line no-undef
// describe('Answer', () => {
//   // Log in to obtain an authentication token before running the tests
//   before((done) => {
//     chai.request(app)
//       .post('/api/v1/auth/login') // Adjust the route if your login route is different
//       .send(userCredentials)
//       .end((err, res) => {
//         authToken = res.body.data.token; // Store the authentication token
//         done();
//       });
//   });

//   // Create a new question to get a questionId for testing the answer creation
//   before((done) => {
//     chai.request(app)
//       .post('/api/v1/questions')
//       .set('Authorization', `Bearer ${authToken}`)
//       .send({
//         question: 'please where is the best place to eat rice and beans in abuja',
//       })
//       .end((err, res) => {
//         questionId = res.body.data.questionId; // Store the questionId for later use
//         done();
//       });
//   });

//   // eslint-disable-next-line no-undef
//   it('should successfully post a new answer', (done) => {
//     chai.request(app)
//       .post(`/api/v1/questions/${questionId}/answers`)
//       .set('Authorization', `Bearer ${authToken}`)
//       .send({
//         answer: 'maybe davido is overrated but who cares if you ask me',
//       })
//       .end((err, res) => {
//         console.log(res.body); // Log the response for inspection

//         // Additional checks
//         expect(res).to.have.status(201);
//         expect(res.body).to.have.property('status').equal('success');
//         expect(res.body).to.have.property('message').equal('answer has been posted');
//         expect(res.body.data).to.have.property('answerId').that.is.a('string');
//         expect(res.body.data).to.have.property('upvotes').to.equal(0);
//         expect(res.body.data).to.have.property('downvotes').to.equal(0);
//         expect(res.body.data).to.have.property('upvotesmeta').to.be.an('array');
//         expect(res.body.data).to.have.property('downvotesmeta').to.be.an('array');
//         expect(res.body.data).to.have.property('preferred').to.equal(false);
// eslint-disable-next-line max-len
//         expect(res.body.data).to.have.property('answer').to.equal('maybe davido is overrated but who cares if you ask me');
//         expect(res.body.data).to.have.property('questionId').to.equal(questionId);
//         expect(res.body.data).to.have.property('updatedAt').to.be.a('string');
//         expect(res.body.data).to.have.property('createdAt').to.be.a('string');

//         done();
//       });
//   });
// });

/* eslint-disable no-undef */
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const { app } = require('../app');

const { expect } = chai;

// eslint-disable-next-line no-undef
describe('Fetch All Answers', () => {
  // eslint-disable-next-line no-undef
  it('should successfully fetch all answers', (done) => {
    chai.request(app)
      .get('/api/v1/questions/0c6bc395-ecf0-423e-bd6e-dfcdb27dea9d')
      .end((err, res) => {
        console.log(res.body); // Log the response for inspection

        // Additional checks
        expect(res.body).to.have.property('status').equal('success');
        expect(res.body).to.have.property('message').equal('answers fetched successfully');
        expect(res.body).to.have.property('data');
        // You can add more detailed checks based on your actual response structure

        done();
      });
  });
});
