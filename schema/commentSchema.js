const Joi = require('joi');

const commentSchema = Joi.object().keys({
    comment: Joi.string()
    .min(30)
    .required()
    .trim()
    .error(new Error('Question should at least 30 characters long')),
});

module.exports = {
    commentSchema,
};
