const Joi = require('joi');

const answerSchema = Joi.object().keys({
    answer: Joi.string()
    .min(30)
    .required()
    .trim()
    .error(new Error('Answer should at least 30 characters long')),
});

module.exports = {
    answerSchema,
};
