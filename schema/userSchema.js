const Joi = require('joi');

const registerSchema = Joi.object().keys({
    firstName: Joi.string()
    .trim()
    .min(3)
    .max(25)
    .required()
    .alphanum()
    .pattern(/^[a-zA-Z]+$/)
    .error(new Error('Please provide a valid first name.')),

    lastName: Joi.string()
    .min(3)
    .max(25)
    .trim()
    .alphanum()
    .required()
    .pattern(/^[a-zA-Z]+$/)
    .error(new Error('Please provide a valid last name.')),

    email: Joi.string()
    .email()
    .required()
    .trim()
    .error(new Error('Please provide a valid email address.')),

    password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/)
    .required()
    .min(7)
    .max(20)
    .error(new Error('Password should be between 7 and 20 characters.')),
});

const loginSchema = Joi.object().keys({
    email: Joi.string().trim()
    .email()
    .lowercase()
    .required()
    .error(new Error('Please provide a valid email address.')),

    password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/)
    .required()
    .min(5)
    .max(20)
    .error(new Error('Please provide a valid password.')),
});

module.exports = {
    registerSchema,
    loginSchema,
};
