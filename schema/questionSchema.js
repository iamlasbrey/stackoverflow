const Joi = require('joi');

const questionSchema = Joi.object().keys({
    question: Joi.string()
    .min(30)
    .required()
    .trim()
    .error(new Error('Question should at least 30 characters long')),
});

module.exports = {
    questionSchema,
};
