# StackOverflow-lite

## Introduction
StackOverflow-lite is a platform where people can ask questions and provide answers.

## Features
•⁠  Account Management: Users can effortlessly create accounts and log in to the platform.

•⁠  Post and Manage Questions: Ask questions and manage them, including the ability to delete your own questions

•⁠  Interactive Q&A Experience: Mark a preferred answer, upvote or downvote responses, and comment on answers.

•⁠  Account Management: Users can effortlessly create accounts and log in to the platform.

•⁠  User Navigation: Fetch a comprehensive list of all questions a user has ever asked on the platform.


## Installation Guide
•⁠  ⁠Clone this repository [here](https://gitlab.com/iamlasbrey/stackoverflow.git).

•⁠  ⁠Run npm install to install all dependencies

•⁠  Create a MySQL database named Stackoverflowlite_2.0.

•⁠  Create an .env file in your project root folder.

•⁠  Set up the following environment variables in a .env file :
* DATABASE
* DATABASE_USERNAME
* DATABASE_PASS
* PORT
* HARSHPASS
* JWTPASS


## Usage
•⁠  Run  **NPM START** to start the application.

•⁠  ⁠Connect to the API using Postman on port 3005.

## API Endpoints
| HTTP Verbs | Endpoints | Action |
| --- | --- | --- |
| POST | /api/v1/auth/login | To login an existing user account|
| POST |  /api/v1/auth/register  | To create a user account |
| GET | /api/v1/questions | To fetch all user questions |
| POST | /api/v1/questions | To create question by a user |
| GET | /api/v1/questions/:questionId | To fetch a specific question |
| DELETE | /api/v1/questions/:questionId | To delete a specific question |
| POST | /api/v1/questions/:questionId/answers | To create an answer for a question |
| POST | /api/v1/questions/accept/:questionId?answerId | To choose a preffered answer |
| POST | /api/v1/questions/upvote/:answerId | To upvote a specific answer |
| POST | /api/v1/questions/downvote/:answerId | To downvote a specific answer |
| POST | /api/v1/answers/comment/:answerId | To comment on a specific answer |


## Technologies Used

•⁠  ⁠[NodeJS](https://nodejs.org/) This is a cross-platform runtime environment built on Chrome's V8 JavaScript engine used in running JavaScript codes on the server. It allows for installation and managing of dependencies and communication with databases.

•⁠  ⁠[ExpressJS](https://www.expresjs.org/) This is a NodeJS web application framework.

•⁠  ⁠[Sequelize](https://sequelize.org//) Sequelize is a modern TypeScript and Node.js ORM for Oracle, Postgres, MySQL, MariaDB, SQLite and SQL Server, and more.


### Authors
•⁠  ⁠[Uzoma Kenkwo] (https://gitlab.com/iamlasbrey/stackoverflow)

### License
This project is available for use under the MIT License.